<?php

namespace Drupal\Tests\adobe_analytics\Unit\TrackingMatcher;

use Drupal\adobe_analytics\TrackingMatcher\RoleContext;
use Drupal\Core\Access\AccessResultAllowed;
use Drupal\Core\Access\AccessResultForbidden;
use Drupal\Core\Session\AccountProxyInterface;
use Drupal\Tests\ConfigTestTrait;
use Drupal\Tests\UnitTestCase;

/**
 * Tests the RoleContext.
 *
 * @group adobe_analytics
 * @coversDefaultClass \Drupal\adobe_analytics\TrackingMatcher\RoleContext
 */
class RoleContextTest extends UnitTestCase {
  use ConfigTestTrait;

  /**
   * Test the access method for various scenarios.
   *
   * @param string $role_tracking_type
   *   The configured type of role tracking. One of 'inclusive' or 'exclusive'.
   * @param array $track_roles
   *   The array of roles to track / not track.
   * @param array $user_roles
   *   The array of roles that the current user has.
   * @param string $access_result
   *   The expected access result for the aforementioned combination of config.
   *
   * @covers ::access
   * @dataProvider accessProvider
   */
  public function testAccess(string $role_tracking_type, array $track_roles, array $user_roles, string $access_result) {
    // Set for anonymous tracking only.
    $config_factory = $this->getConfigFactoryStub([
      'adobe_analytics.settings' => [
        'role_tracking_type' => $role_tracking_type,
        'track_roles' => $track_roles,
      ],
    ]);

    // Mock admin user.
    $current_user = $this->createMock(AccountProxyInterface::class);
    $current_user->method('getRoles')
      ->willReturn($user_roles);

    $role_context = new RoleContext($config_factory, $current_user);
    $this->assertInstanceOf($access_result, $role_context->access());
  }

  /**
   * Data provider for the testAccess test.
   */
  public static function accessProvider(): array {
    return [
      [
        'inclusive',
        [
          'anonymous' => 'anonymous',
          'authenticated' => '0',
          'administrator' => '0',
        ],
        [
          'authenticated',
          'administrator',
        ],
        AccessResultForbidden::class,
      ],
      [
        'inclusive',
        [
          'anonymous' => 'anonymous',
          'authenticated' => '0',
          'administrator' => '0',
        ],
        [
          'anonymous',
        ],
        AccessResultAllowed::class,
      ],
      [
        'exclusive',
        [
          'anonymous' => '0',
          'authenticated' => '0',
          'administrator' => 'administrator',
        ],
        [
          'anonymous',
        ],
        AccessResultAllowed::class,
      ],
      [
        'exclusive',
        [
          'anonymous' => '0',
          'authenticated' => '0',
          'administrator' => 'administrator',
        ],
        [
          'authenticated',
          'administrator',
        ],
        AccessResultForbidden::class,
      ],
    ];
  }

}
